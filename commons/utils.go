package commons

import (
	"encoding/json"
)

func ToStruct(source interface{}, to interface{}) error {
	tmp, err := json.Marshal(source)
	if err != nil {
		return err
	}
	err = json.Unmarshal(tmp, to)
	if err != nil {
		return err
	}
	return nil
}

func StringArrayContains(target string, list []string) bool {
	for _, item := range list {
		if item == target {
			return true
		}
	}
	return false
}
