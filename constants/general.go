package constants

// AppName : このアプリケーションの名前
const AppName = "CSVJSONCnverter"

// LowerAppName : 小文字にしたこのアプリケーションの名前
const LowerAppName = "csv-json-converter"

// Version : このアプリケーションのバージョン
const Version = "v0.0.1"
