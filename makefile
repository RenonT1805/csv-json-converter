run:
	go build
	csv-json-converter

run-sample:
	go build
	csv-json-converter.exe to-json --csv=sample.csv --struct=sample.json --separate=',' --out=result.json