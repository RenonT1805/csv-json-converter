package commands

import (
	"csv-json-converter/constants"
	"csv-json-converter/to_json"
	"errors"
	"flag"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

// ToJson : CSVからJSONへ変換するコマンド
type ToJson struct {
	Flags             *flag.FlagSet
	csvSource         string
	jsonStructure     string
	outPath           string
	separateCharacter string
}

func (t ToJson) showCommandUsage() {
	out := log.StandardLogger().Out
	flag.CommandLine.SetOutput(out)
	fmt.Fprintf(out, "\n使い方: %s %s [options...]\n", constants.LowerAppName, t.CommandName())
	fmt.Fprintf(out, "\n説明: %s\n", t.CommandDescription())
	fmt.Fprintf(out, "\n実行オプション:\n")
	t.Flags.PrintDefaults()
}

// initialize : コマンドの初期化
func (t *ToJson) initialize(args []string) error {
	t.Flags = flag.NewFlagSet(t.CommandName(), flag.ExitOnError)
	t.Flags.Usage = t.showCommandUsage
	t.Flags.StringVar(&t.outPath, "out", "", "変換後のJSONファイルのパスを指定します")
	t.Flags.StringVar(&t.csvSource, "csv", "", "変換元のCSVファイルのパスを指定します(必須)")
	t.Flags.StringVar(&t.jsonStructure, "struct", "", "変換対象のJSON構造のパスを指定します(必須)")
	t.Flags.StringVar(&t.separateCharacter, "separate", ",", "CSVの分割文字を設定します")
	err := t.Flags.Parse(args)
	if err != nil {
		return err
	}

	if t.csvSource == "" {
		return errors.New("csvの指定は必須です")
	}

	if t.jsonStructure == "" {
		return errors.New("structの指定は必須です")
	}

	if len(t.separateCharacter) != 1 {
		return errors.New("CSVの分割文字は必ず一文字で設定してください")
	}
	return nil
}

// CommandName : コマンド名を返す
func (t ToJson) CommandName() string {
	return "to-json"
}

// CommandDescription : コマンド説明を返す
func (t ToJson) CommandDescription() string {
	return "CSVデータをJSONデータへ変換します"
}

// Run : コマンドの実行
func (t ToJson) Run(args []string) constants.ResponseCode {
	err := t.initialize(args)
	if err != nil {
		log.Errorln(err)
		return constants.Failed
	}

	csvFile, err := os.Open(t.csvSource)
	if err != nil {
		log.Errorln(err)
		return constants.Failed
	}
	defer csvFile.Close()

	structureFile, err := os.Open(t.jsonStructure)
	if err != nil {
		log.Errorln(err)
		return constants.Failed
	}
	defer structureFile.Close()

	result, err := to_json.CSVToJSONStringFromJSONStructure(csvFile, structureFile, rune(t.separateCharacter[0]))
	if err != nil {
		log.Errorln(err)
		return constants.Failed
	}

	if t.outPath == "" {
		_, err = fmt.Fprintf(log.StandardLogger().Out, "\n%s\n", result)
		if err != nil {
			log.Errorln(err)
			return constants.Failed
		}
	} else {
		file, err := os.OpenFile(t.outPath, os.O_WRONLY|os.O_CREATE, 0666)
		file.Seek(0, 0)
		if err != nil {
			log.Errorln(err)
			return constants.Failed
		}
		defer file.Close()

		_, err = fmt.Fprintf(file, "%s", result)
		if err != nil {
			log.Errorln(err)
			return constants.Failed
		}
	}
	return constants.Successful
}
