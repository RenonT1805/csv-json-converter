package to_json

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"io"
	"strings"

	"github.com/sirupsen/logrus"
)

func createObjectStyleJson(dataCount int, structureJSON map[string]interface{}, csvMap map[string][]string) (map[string]interface{}, error) {
	if len(structureJSON) == 0 {
		return map[string]interface{}{}, nil
	}

	var (
		keyTemplate string
		template    []byte
		err         error
	)
	for key, item := range structureJSON {
		keyTemplate = key
		template, err = json.Marshal(item)
		if err != nil {
			return nil, err
		}
		break
	}

	structureJSON = map[string]interface{}{}
	for idx := 0; idx < dataCount; idx++ {
		structureString := string(template)
		newKey := keyTemplate
		for key, item := range csvMap {
			keyWord := "{{" + key + "}}"
			if newKey == keyWord {
				newKey = item[idx]
			}

			if strings.Contains(structureString, keyWord) {
				structureString = strings.ReplaceAll(structureString, keyWord, item[idx])
			}
		}

		var buf interface{}
		json.Unmarshal([]byte(structureString), &buf)
		structureJSON[newKey] = buf
	}
	return structureJSON, nil
}

func createArrayStyleJson(dataCount int, structureJSON []interface{}, csvMap map[string][]string) ([]interface{}, error) {
	if len(structureJSON) == 0 {
		return []interface{}{}, nil
	}

	template, err := json.Marshal(structureJSON[0])
	if err != nil {
		return nil, err
	}

	structureJSON = []interface{}{}
	for idx := 0; idx < dataCount; idx++ {
		structureString := string(template)
		for key, item := range csvMap {
			keyWord := "{{" + key + "}}"
			if strings.Contains(structureString, keyWord) {
				structureString = strings.ReplaceAll(structureString, keyWord, item[idx])
			}
		}

		var buf interface{}
		json.Unmarshal([]byte(structureString), &buf)
		structureJSON = append(structureJSON, buf)
	}
	return structureJSON, nil
}

func CSVToJSONStringFromJSONStructure(csvSource, jsonStructure io.Reader, separateCharacter rune) (string, error) {
	r := csv.NewReader(csvSource)
	r.Comma = separateCharacter

	csvMap := map[string][]string{}
	headers, err := r.Read()
	var dataCount int
	if err != nil {
		logrus.Infoln("CSVファイルの読み込みに失敗しました")
		return "", err
	}
	for _, header := range headers {
		csvMap[header] = []string{}
	}

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.Infoln("CSVデータの読み込みに失敗しました")
			return "", err
		}

		for idx, item := range record {
			csvMap[headers[idx]] = append(csvMap[headers[idx]], item)
		}
		dataCount++
	}

	bytes, err := io.ReadAll(jsonStructure)
	if err != nil {
		logrus.Infoln("JSON構造ファイルの読み込みに失敗しました")
		return "", err
	}

	var structureJson interface{}
	json.Unmarshal(bytes, &structureJson)
	if err != nil {
		logrus.Infoln("JSON構造が不正です")
		return "", err
	}

	var resultJson interface{}
	if v, castable := structureJson.([]interface{}); castable {
		resultJson, err = createArrayStyleJson(dataCount, v, csvMap)
	} else if v, castable := structureJson.(map[string]interface{}); castable {
		resultJson, err = createObjectStyleJson(dataCount, v, csvMap)
	} else {
		return "", errors.New("CSVデータから指定されたJSON構造へ変換できません")
	}

	bytes, err = json.Marshal(resultJson)
	if err != nil {
		logrus.Infoln("JSONオブジェクトを文字列に変換できませんでした")
		return "", err
	}

	return string(bytes), nil
}
