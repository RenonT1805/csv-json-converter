module csv-json-converter

go 1.16

require (
	github.com/mattn/go-colorable v0.1.8
	github.com/rs/xid v1.3.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
